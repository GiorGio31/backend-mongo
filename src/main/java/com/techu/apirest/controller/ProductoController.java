package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel prod){
        if (productoService.existsById(prod.getId()))
            return new ProductoModel("0","id existente",0.0);
        else
            return productoService.save(prod);
    }

    @PutMapping("/productos")
    public boolean putProductos(@RequestBody ProductoModel productoactualizar){
        if (productoService.existsById(productoactualizar.getId())) {
            productoService.save(productoactualizar);
            return true;
        }
        else
            return false;
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productodelete){
        return productoService.delete(productodelete);
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductos(@PathVariable String id) {
        if (productoService.existsById(id))
        {
            productoService.deleteById(id);
            return true;
        }
        else
            return false;
    }


    /*
    * El metodo patch recibe un body con formato json usando MAP para clave-valor, por ejemplo:
    * {
    *   "descripcion" : "nueva descripcion"
    *   "precio" : 100.0
    * }
    * También funciona con solamente uno de los valores
    * {
    *   "precio" : 100.0
    * }
    * */
    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductosId(@RequestBody Map<String, Object> update, @PathVariable String id) {

        ProductoModel productopatchear;
        Optional<ProductoModel> oproducto = productoService.findById(id);
        if (oproducto.isPresent()){
            productopatchear = oproducto.get();
        }
        else
            return new ProductoModel("0","id no existente",0.0);;
        if (update.containsKey("precio"))
            productopatchear.setPrecio((Double) update.get("precio"));
        if(update.containsKey("descripcion"))
            productopatchear.setDescripcion((String) update.get("descripcion"));

        productoService.save(productopatchear);
        return productopatchear;
    }






}
